# GitOps con ArgoCD

Este curso está desarrollado con [reveal-md](https://github.com/webpro/reveal-md).
Los comandos básicos para operar son:

* **`npm start`:** Modo presentación
* **`npm run-script print`:** Imprime la presentación como PDF

## Ejemplos

Los ejemplos se corren usando [kind](https://kind.sigs.k8s.io/docs/user/quick-start/).

Para simplificar las configuraciones requeridas, se recomienda utilizar
[direnv](https://direnv.net/) y de esta forma toda configuración utilizada será
local a los directorios en este repositorio.

```
cd src/examples
kind-up
```

### asciinema de los ejemplos

Los videos generados con `asciinema` están en `src/static/videos`

#### Video 01: iniciar cluster

```
cd src/examples
direnv allow
kind-up
kubectl get ns
```

#### Video 02: instalar Argo CD

```
kubectl create ns argocd
helm repo add argo https://argoproj.github.io/argo-helm
helm repo update
helm install argocd argo/argo-cd -f config/argocd.yaml -n argocd
kubectl -n argocd get all
```

#### Acceso a la UI

```
kubectl -n argocd port-forward service/argocd-server 4430:443
```

#### ¿Cómo podemos ver los ejemplos que inicinamos con Argo?

Los ejemplos inician servicios de tipo `NodePort`, entonces podemos acceder a
ellos usando la IP de cualquiera de los nodos y el puerto creado en forma
aleatoria. 

Para conocer la IP de cualquiera de los nodos usar:

```
kubectl get nodes -o wide
```
