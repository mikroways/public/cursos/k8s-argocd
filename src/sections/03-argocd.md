# Argo CD

<img class="main" height="180px" src="static/argo-logo.png" />
----
## Instalación

Para este curso, y a modo de ejemplo, utilizaremos [kind](https://kind.sigs.k8s.io/docs/user/quick-start/)
(Kubernetes in Docker). Para ello, hemos preparado el ambiente como se indica en
el README de la actual presentación:

https://bit.ly/2Vw3vlY

----

## Iniciando el cluster

<asciinema-player src="static/videos/01-start-cluster.cast" speed="7" poster="npt:0:50" theme="tango" rows="20" cols="140"></asciinema-player>
----
## Instalando Argo CD

<asciinema-player src="static/videos/02-install-argocd.cast" speed="2" poster="npt:0:12" theme="tango" rows="20" cols="140"></asciinema-player>

----

## Conectando con Argo CD

Para acceder a la UI, tenemos varias alternativas que tienen que ver con qué
tipo de ingress utilizaremos (LB o Ingress). Para no complicarnos en esta
instancia, utilizaremos `port-forward` para acceder a la UI:

```bash
kubectl -n argocd port-forward svc/argocd-server 4430:443
```

> Redirección de https://localhost:4430 al servicio de argo

----
## Acceso

<img class="main" height="400px" src="static/argo-login.png" />

> ¿Y qué usuario y contraseña?

----

## Usuario y contraseña

Por defecto, la contraseña de admin coincide con el nombre del pod
correspondiente al servidor de argo:

```
kubectl get pods \
    -n argocd \
    -l app.kubernetes.io/name=argocd-server \
    -o name | cut -d'/' -f 2
```
----
## Argo CD UI

<img class="main" height="400px" src="static/argo-landing-page.png" />

----
## Conceptos

Argo CD introduce una serie de conceptos con los que debemos familiarizarnos
para poder utilizar Argo:

* Proyectos
* Aplicaciones
* Repositorios

----

<!-- .slide: class="list medium" -->
## Proyectos

Permiten agrupar las aplicaciones lógicamente. Esto ofrece versatilidad a la
hora de trabajar con Argo por diferentes equipos. Los proyectos ofrecen las
siguientes características:

* Restringir **qué** puede desplegarse (repositorios GIT confiables)
* Restringir **dónde** las aplicaciones pueden desplegarse (clusters y namespaces)
* Restringir **qué tipo de objetos** pueden o no desplegarse (RBAC, CRD,
  daemonsets, etc).
* Definir roles por proyectos que proveen RBAC a las aplicaciones. **Requiere
  integración con openid-connect**.

----

## Aplicaciones

Representan un conjunto de recursos de Kubernetes definidos por sus manifiestos.
Las aplicaciones serán de un tipo de fuente específico que definirá qué
herramienta utilizará argo para generar los manifiestos:

* [Helm](https://helm.sh/)
* [Kustomize](https://kustomize.io/)

----

## Repositorios

Un repositorio representa aquellos repositorios privados en los cuales se
necesita especificar un usuario y contraseña para su utilización. 

Para ello, Argo CD provee mecanismos para especificar credenciales tanto por
https como ssh.

