# Introducción

----

## ¿A qué llamamos automatización?

En DevOps se define una serie de principios que deben cumplirse, entre los que
se destaca la automatización. 

La automatización puede tomar dos formas:

* **Infraestructura subyacente**
* **Desarrollo de aplicaciones y despliegue**

----
## Beneficios de automatizar

* Se reducen los costos.
* Se acelera la ejecución de cualquier proceso sobre la infraestructura.
  * _Instalar un servidor nuevo, modificar un load balancer, actualizar una
    aplicación o actualizar los certificados de múltiples servidores._
* Minimizar errores humanos o problemas de seguridad inherentes al proceso
  manual.

----

# Infraestructura como Código

----

## Infraestructura como código (IaC)

* Proceso de manejo y aprovisionamiento de servidores mediante código.
* Promueve la ejecución desantendida, evitando la interactivad.
* Evita el uso de documentación: _el código documenta la infraestructura._
* Idempotencia.
* Se adoptan prácticas del desarrollo:
  * Versionado del código
  * Testing
  * Colaboración (uso de librerías)

----

## Usos de IaC

En general, IaC se utiliza para configurar la infraestructura subyacente, esto
es, los servicios que se requieren instalar en los servidores, y no son una
solución **_cómoda para el despliegue de aplicaciones_**.

> Esto no significa que no se utilicen para el despliegue continuo de
> aplicaciones, sino que su uso no es tan amigable como otras soluciones.

----

## Herramientas populares de IaC

* [Ansible](https://www.ansible.com/)
* [Chef](https://www.chef.io/)
* [CloudFormation](https://aws.amazon.com/es/cloudformation/)
* [Puppet](https://puppet.com/)
* [Saltstack](https://www.saltstack.com/)
* [Terraform](https://www.terraform.io/)

----

## Herramientas para despliegues

Existen herramientas específicamente desarrolladas para simplificar el 
despliegue de aplicaciones.

Las mismas están diseñadas para preparar un
(o múltiples) servidor(es) para que una aplicación se instale en ellos.

### Herramientas de despliegue:

* [Capistrano](https://capistranorb.com/)
* [dpl](https://github.com/travis-ci/dpl)

----

## Herramientas para despliegues

Estas herramientas han ido evolucionando con los años, pero su concepción
original fue la simplificación del despliegue en servidores físicos o PaaS.

Hoy día, con la explosión de los contenedores, la forma de realizar despliegues
y entregas han tomado un giro de 180 grados, simplificando drásticamente la
problemática de los despliegues.

----

# GitOps

----
<!-- .slide: class="list medium" -->
## ¿Qué es GitOps?

hay mucha mística alrededor de los términos emergentes. Es interesante la
[siguiente definición](https://twitter.com/vitorsilva/status/999978906903080961)
de tres oraciones acerca de GitOps:

* GIT como **única** fuente de confianza del sistema.
* GIT como **único** lugar donde se manipulan (creación, modificación y destrucción) 
  de **todos** los ambientes.
* **Todos los cambios son observables y verificables**.

<small style="margin-top: 30px">
<em>
Es importante destacar que se asume un entendimiento de GIT, y de los flujos
de trabajo con Git como por ejemplo git flow, feature branches, pull/merge
requests, entre otros.
</em>
<br />
Estos conceptos se cubren en la capacitación de <em><b>Workflow continuo de
desarrollo a producción</em></b> dictada por Mikroways.
</small>
----

## GitOps y Kubernetes

Si bien es posible utilizar el concepto de GitOps en [cualquier esquema](https://www.redhat.com/sysadmin/gitops-git-add),
existe una gran afinidad con Kubernetes.

Esta afinidad radica en que Kubernetes configura sus objetos de forma
declarativa mediante YAML o JSON. Incluso se crearon nuevas herramientas que
simplifica la gestión de dichos objetos de forma de poder templetizarlos.

----
## GitOps y Kubernetes

<b>Justamente en este punto es donde GitOps entra en juego</b>, garantizando que
la definición de los objetos de kubernetes se almacene en Git de forma de
mantener sincronizadas nuestras definiciones con el cluster de kubernetes
(convergencia).

----
## GitOps sin kubernetes

> Es importante destacar que podemos aplicar GitOps con cada uno de los
> productos mencionados en IaC (chef, ansible, terraform, etc).

----
# Tipos de despliegue en Kubernetes
----
## Tipos de despliegue en Kubernetes

Como mencionamos anteriormente, k8s soporta definiciones declarativas de sus
objetos utilizando YAML o JSON.

Una forma de simplificar la gestión de estas definiciones, es utilizar
herramientas que simplifiquen la gestión:

* [Helm charts](https://helm.sh/)
* [Kustomize](https://kustomize.io/)
* [Jsonnet](https://jsonnet.org/articles/kubernetes.html)
* ~[Ksonnet](https://ksonnet.io/)~
----
## Consideraciones K8S

Las aplicaciones desplegadas en K8S seguramente sea objetos `Deployment` o
`StatefulSet`. Se asume un entendimiento de las estrategias de despliegue
provistas por Kubernetes para estos objetos, así como la influencia del tipo de
volúmenes utilizados por los `Pods` para que sea posible un despliegue sin
problemas.

<small style="margin-top: 30px">
Estos conceptos se cubren en la capacitación de <em><b>Introducción a Kubernetes
</em></b> dictada por Mikroways.
</small>
