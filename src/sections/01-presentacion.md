## Presentación
----
### Sobre Mikroways

* Brindamos servicios de TI, específicamente relacionados a la infraestructura
  tecnológica de nuestros clientes.
* Diseñamos e implementamos soluciones a medida según las necesidades de cada
  cliente.
* Damos soporte para solucionar cualquier problemática que surja en el
  funcionamiento de la infraestructura tecnológica existente.
* Proveemos capacitación al personal de sistemas, adaptando los cursos a los
  requerimientos específicos.
----
## Nuestros servicios

* DevOps
* Monitoreo & Logs
* Docker & Kubernetes & Openshift
* Entrega y Despliegue contínuo. GitOps
* Cloud Computing
* Escalabilidad de aplicaciones web
* Capacitaciones

----

## Agenda

- Introduccón
  - Infraestructura como código
  - GitOps
  - Tipos de despliegue en K8S
- Argo CD
  - Instalación
  - Conceptos
  - Uso de la UI
- Ejemplos
----

## Conocimientos previos ideales

- SysAdmin / Dev
- Servicios
- Contenedores / docker
- K8S
- Kustomize, Helm
- JSON / YAML

----

## Recomendación

> Las capacitaciones de **Workflow contínuo de desarrollo a producción** e
> **Introducción a Kubernetes**, dictadas por Mikroways son recomendadas para un
> mejor entendimiento de este curso.
