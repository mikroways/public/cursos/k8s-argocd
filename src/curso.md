---
title: GitOps con Argo CD
theme: league
highlightTheme: vs2015
preprocessor: preproc.js
revealOptions:
  transition: 'slide'
---

# GitOps con Argo CD

<div class="container" style="margin-top: 200px">
  <div class="col">
    <img class="main" height="150px" src="static/k8s.png" />
  </div>
  <div class="col">
    <img class="main" style="padding-top:50px" height="60px" src="static/logo-nombre-transparente-blanco.png" />
  </div>
  <div class="col">
    <img class="main" height="180px" src="static/argo-logo.png" />
  </div>
</div>

https://bit.ly/3eS0UcF

---

FILE: 01-presentacion.md

---

FILE: 02-introduccion.md

---

FILE: 03-argocd.md

---

FILE: 04-argocd-app.md

